// examples
db.solarSystem.find({type: {$ne: "Star" } });
db.solarSystem.aggregate([{ $match: { type: { $ne: "Star" } } }]);

db.movies.find( { }).count();

db.movies.findOne();

db.movies.find( { "imdb.rating" : {  $gte: 7 } } ).count();
// 15209 documents

// db.movies.find({ genres: {$ne: "Crime", $ne: "Horror"}}).count();

db.movies.find({ $and : [ {genres: {$ne:"Crime"}} , { genres: {$ne: "Horror"}} ]} ,{title:1, genres: 1}  );

db.movies.find({ $and : [ {genres: {$ne:"Crime"}} , { genres: {$ne: "Horror"}} ]} ,{title:1, genres: 1}  ).count();
// 35114 documents

db.movies.find({ $and : [ {genres: {$ne:"Crime"}} , { genres: {$ne: "Horror"}} ,{ "imdb.rating" : {  $gte: 7 } }  ] }   ,{title:1, genres: 1}  ).count();
// 13144 documents

db.movies.find({ $and : [ {genres: {$ne:"Crime"}} , { genres: {$ne: "Horror"}} ,{ "imdb.rating" : {  $gte: 7 } },{languages: {$eq:"English"}},{languages: {$eq: "Japanese"}}   ] }   ,{title:1, genres: 1, languages: 1}  ).count();
// 209 documents

db.movies.find({ $and : [ {genres: {$ne:"Crime"}} , { genres: {$ne: "Horror"}} ,{ "imdb.rating" : {  $gte: 7 } },{languages: {$eq:"English"}},{languages: {$eq: "Japanese"}}, {rated:"PG"}  ] }, {title:1,genres:1,rated:1}     ).count();
// 14 documents

db.movies.find({ $and : [ {genres: {$ne:"Crime"}} , { genres: {$ne: "Horror"}} ,{ "imdb.rating" : {  $gte: 7 } },{languages: {$eq:"English"}},{languages: {$eq: "Japanese"}}, {rated:"G"}  ] }, {title:1,genres:1,rated:1}     ).count();
// 9  documents


db.movies.find( { $and : [ {genres: {$ne:"Crime"}} , { genres: {$ne: "Horror"}} ,{ "imdb.rating" : {  $gte: 7 } },{languages: {$eq:"English"}},{languages: {$eq: "Japanese"}} ,{ $or: [ {rated: "PG"},{rated: "G"}] } ] }, {title:1,genres:1,rated:1}     ).count();
// 23 documents

// equivalent aggregation
db.movies.aggregate( [ {$match: { $and : [ {genres: {$ne:"Crime"}} , { genres: {$ne: "Horror"}} ,{ "imdb.rating" : {  $gte: 7 } },{languages: {$eq:"English"}},{languages: {$eq: "Japanese"}} ,{ $or: [ {rated: "PG"},{rated: "G"}] } ] } }, {$count: "titles"} ] ); 

db.movies.aggregate( [ {$match: { $and : [ {genres: {$ne:"Crime"}} , { genres: {$ne: "Horror"}} ,{ "imdb.rating" : {  $gte: 7 } },{languages: {$eq:"English"}},{languages: {$eq: "Japanese"}} ,{ $or: [ {rated: "PG"},{rated: "G"}] } ] } } ] ); 



