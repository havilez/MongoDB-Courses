# Student Getting Started Guide

Hello and welcome! We're excited to bring this training on "Getting Started with Data Analytics with MongoDB" to you.

We've tried to keep dependencies to a minimum for this workshop.

However, you still need to install few things on your machine.

You need to pick one of the following methods on how to install and run the needed tools:

1. Vagrant Virtual Machine
2. Docker Containers
3. Local installation of the tools.

We've created pre-packaged environments for the first two, both included in
the analytics-student.tgz file

Follow the instructions below for your elected method.


## Vagrant

To use the Vagrant environment, cd to the top directory of the package where you'll find a Vagrantfile.
Type

vagrant up

Next, access the workshop environment by visiting:

http://localhost:9999

in your web browser, preferably Chrome.
The password is: mongo

Click on the "analytics-student.ipynb" in file explorer in your browser to load the workshop notebook!


## Docker

To use the Docker environment, cd to the top directory of the package where you'll find a Dockerfile and docker-compose.yml file.
Type

docker-compose -f ./docker-compose.yml up -d --build

Next, access the workshop environment by visiting:

http://localhost:9999

in your web browser, preferably Chrome.
The password is: mongo

Click on the "analytics-student.ipynb" in file explorer in your browser to load the workshop notebook!


## Local

You can run the workshop from a local environment if you prefer.

There is a hard requirement for python 3 and MongoDB 3.6+.
To install Python 3 on your system, we recommend you use Anaconda. Specific installation instructions can be found at the website. Once you have python 3 installed and ready (verify by typing python --version and it should say 3.6.x)

Next, install the necessary python modules via:

pip install -r requirements.txt

You'll also need to import the dataset needed for this workshop. You can download the dataset from:

https://s3.amazonaws.com/mongodb-training/workshops/dump.tgz

Expand dump.tgz and import the dataset into a running mongod server process with:

mongorestore dump --gzip

Once you have a running MongoDB instance with the data imported in it, you can launch the workshop materials with:

jupyter lab

It should open a browser window for you.
The password is: mongo

Click on the "analytics-student.ipynb" in file explorer in your browser to load the workshop notebook!


# Additional information and tips

If you are unfamiliar with the Jupyter environment, you may find the following link helpful:

http://jupyterlab.readthedocs.io/en/stable/user/interface.html

One last note, prior to editing the notebook, **make a copy**. This way you can
revert changes without having to reprovision!

# Summary

We look forward to this in-depth training session and will see you in New York!
